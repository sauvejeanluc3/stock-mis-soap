package com.rca.stocksoapapi.soap.repository;

import com.rca.stocksoapapi.soap.bean.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ISupplierRepository extends JpaRepository<Supplier,Long> {
}
