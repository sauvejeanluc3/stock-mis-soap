package com.rca.stocksoapapi.soap.bean;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Supplier{

    @Id
    private Long id;

    private String name;

    private String email;

    private String mobile;
}
