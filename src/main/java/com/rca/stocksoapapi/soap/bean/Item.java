package com.rca.stocksoapapi.soap.bean;

import com.rca.stocksoapapi.items.Status;
import com.rca.stocksoapapi.soap.enums.EStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Item {

    @Id
    private Long id;

    private String name;

    private String itemCode;

    @Enumerated(EnumType.STRING)
    private Status status;

    private float price;

    @OneToOne
    private Supplier supplier;


}
