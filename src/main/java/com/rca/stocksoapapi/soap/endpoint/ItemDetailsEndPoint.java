package com.rca.stocksoapapi.soap.endpoint;

import com.rca.stocksoapapi.items.*;
import com.rca.stocksoapapi.soap.bean.Item;
import com.rca.stocksoapapi.soap.bean.Supplier;
import com.rca.stocksoapapi.soap.enums.EStatus;
import com.rca.stocksoapapi.soap.repository.IItemRepository;
import com.rca.stocksoapapi.soap.repository.ISupplierRepository;
import com.rca.stocksoapapi.suppliers.SupplierDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemDetailsEndPoint {


    @Autowired
    private IItemRepository itemRepository;

    @Autowired
    private ISupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://stocksoapapi.rca.com/items", localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request) {

        Item item = itemRepository.findById(request.getId()).get();

        GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
        return  itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://stocksoapapi.rca.com/items", localPart = "GetAllItemDetailsRequest")
    @ResponsePayload
    public GetAllItemDetailsResponse findAll(@RequestPayload GetAllItemDetailsRequest request){
        GetAllItemDetailsResponse allItemDetailsResponse = new GetAllItemDetailsResponse();

        List<Item> items = itemRepository.findAll();
        for (Item item: items){
            GetItemDetailsResponse itemDetailsResponse = mapItemDetails(item);
            allItemDetailsResponse.getItemDetails().add(itemDetailsResponse.getItemDetails());
        }
        return allItemDetailsResponse;
    }


    @PayloadRoot(namespace = "http://stocksoapapi.rca.com/items", localPart = "CreateItemDetailsRequest")
    @ResponsePayload
    public CreateItemDetailsResponse save(@RequestPayload CreateItemDetailsRequest request) {
        Supplier supplier = supplierRepository.findById(request.getItemDetails().getId()).get();

        itemRepository.save(new Item(request.getItemDetails().getId(),
                request.getItemDetails().getName(),
                request.getItemDetails().getItemCode(),
                request.getItemDetails().getStatus(),
                request.getItemDetails().getPrice(),
                supplier
        ));

        CreateItemDetailsResponse itemDetailsResponse = new CreateItemDetailsResponse();
        itemDetailsResponse.setItemDetails(request.getItemDetails());
        itemDetailsResponse.setMessage("Created Successfully");
        return itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://stocksoapapi.rca.com/items", localPart = "UpdateItemDetailsRequest")
    @ResponsePayload
    public UpdateItemDetailsResponse update(@RequestPayload UpdateItemDetailsRequest request) {
        UpdateItemDetailsResponse itemDetailsResponse = null;
        Optional<Item> existingItem = this.itemRepository.findById(request.getItemDetails().getId());
        if(existingItem.isEmpty() || existingItem == null) {
            itemDetailsResponse = mapItemDetail(null, "Id not found");
        }
        if(existingItem.isPresent()) {

            Item _Item = existingItem.get();
            _Item.setName(request.getItemDetails().getName());
            _Item.setItemCode(request.getItemDetails().getItemCode());
            _Item.setStatus(request.getItemDetails().getStatus());
            _Item.setPrice(request.getItemDetails().getPrice());
            _Item.setSupplier(
                    new Supplier(
                            request.getItemDetails().getSupplierDetails().getId(),
                            request.getItemDetails().getSupplierDetails().getName(),
                            request.getItemDetails().getSupplierDetails().getEmail(),
                            request.getItemDetails().getSupplierDetails().getMobile()
                    )
            );
            itemRepository.save(_Item);
            itemDetailsResponse = mapItemDetail(_Item, "Updated successfully");

        }
        return itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://stocksoapapi.rca.com/items", localPart = "DeleteItemDetailsRequest")
    @ResponsePayload
    public DeleteItemDetailsResponse delete(@RequestPayload DeleteItemDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        itemRepository.deleteById(request.getId());

        DeleteItemDetailsResponse itemDetailsResponse = new DeleteItemDetailsResponse();
        itemDetailsResponse.setMessage("Deleted Successfully");
        return itemDetailsResponse;
    }

    private GetItemDetailsResponse mapItemDetails(Item item){
        ItemDetails itemDetails = mapItem(item);

        GetItemDetailsResponse itemDetailsResponse = new GetItemDetailsResponse();

        itemDetailsResponse.setItemDetails(itemDetails);
        return itemDetailsResponse;
    }

    private UpdateItemDetailsResponse mapItemDetail(Item item, String message) {
        ItemDetails itemDetails = mapItem(item);
        UpdateItemDetailsResponse itemDetailsResponse = new UpdateItemDetailsResponse();

        itemDetailsResponse.setItemDetails(itemDetails);
        itemDetailsResponse.setMessage(message);
        return itemDetailsResponse;
    }

    private ItemDetails mapItem(Item item){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setId(item.getId());
        itemDetails.setName(item.getName());
        itemDetails.setItemCode(item.getItemCode());
        itemDetails.setStatus(item.getStatus());
        itemDetails.setPrice(item.getPrice());

        SupplierDetails newSupplierDetails = new SupplierDetails();
        newSupplierDetails.setId(item.getSupplier().getId());
        newSupplierDetails.setName(item.getSupplier().getName());
        newSupplierDetails.setEmail(item.getSupplier().getEmail());
        newSupplierDetails.setMobile(item.getSupplier().getMobile());

        itemDetails.setSupplierDetails(newSupplierDetails);


        return itemDetails;
    }
}

